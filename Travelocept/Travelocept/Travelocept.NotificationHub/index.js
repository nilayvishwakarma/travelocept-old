const express = require("express");
const app = express();
app.use(express.static(__dirname));

const http = require("http").Server(app);

const bodyParser = require("body-parser");
// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));
// parse requests of content-type - application/json
app.use(bodyParser.json());

// Configure MongoDb ======================================
const mongoose = require("mongoose");
mongoose.Promise = global.Promise;
// Connecting to the database
mongoose
  .connect(
    require("./config/database.config.js").url,
    {
      useNewUrlParser: true
    }
  )
  .then(() => {
    console.log("Successfully connected to the database");
  })
  .catch(err => {
    console.log("Could not connect to the database. Exiting now...", err);
    process.exit();
  });

// Configure Routes ======================================
// add notification route
require("./routes/notification.route")(app);

// define a simple route
app.get("/", (req, res) => {
  res.sendFile("index.html");
});
// listen for requests
http.listen(3000, () => {
  console.log("Travelocept Notification Hub is listening on port 3000");
});

// Configure Socket ======================================

const io = require("socket.io")(http);

// Socket rooms
io.sockets.on("connection", socket => {
  console.log("A client connected");

  socket.on("join", data => {
    console.log(data);
    // Request to join a private room
    socket.join(data);
    io.sockets
      .in(data)
      .emit("new_msg", { msg: "Hi " + data + " Joined a private room!" });
  });

  socket.on("disconnect", function() {
    console.log("A client disconnected");
  });

  //Send a message after a timeout of 4seconds
  setTimeout(function() {
    socket.broadcast.emit('new_msg', "Sent a message 4seconds after connection!")
    socket.send("Sent a message 4seconds after connection!");
  }, 4000);
});
