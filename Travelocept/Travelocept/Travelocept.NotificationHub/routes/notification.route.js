module.exports = (app) => {
    const notifications = require("../controllers/notification.controller");

    // Create a new notification
    app.post('/notifications', notifications.create);

    // Retrieve all notifications
    app.get('/notifications', notifications.findAll);

    // Retrieve a single notification with notificationId
    app.get('/notifications/:notificationId', notifications.findOne);

    // Update a notification with notificationId
    app.put('/notifications/:notificationId', notifications.update);

    // Delete a notification with notificationId
    app.delete('/notifications/:notificationId', notifications.delete);
}