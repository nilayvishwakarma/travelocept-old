const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let NotificationSchema = new Schema(
  {
    from: { type: String, required: true, max: 30, default: "*" },
    for: { type: String, required: true, max: 30, default: "*" },
    payload: { type: String, required: true, default: JSON.stringify({}) },
    acknowledged: { type: Boolean, required: true, default: false }
  },
  {
    timestamps: true
  }
);

module.exports = mongoose.model("notification", NotificationSchema);
