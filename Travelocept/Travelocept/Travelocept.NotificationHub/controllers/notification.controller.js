const Notification = require("../models/notification.model");

// Create and Save a new notification
exports.create = (req, res) => {
  // Validate request
  if (!req.body) {
    return res.status(400).send({
      message: "Notification content can not be empty"
    });
  }

  // Create a Notification
  const notification = new Notification({
    for: req.body.for || "*",
    payload: req.body.payload,
    acknowledged: false
  });

  // Save Notification in the database
  notification
    .save()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the notification."
      });
    });
};

// Retrieve and return all notifications from the database.
exports.findAll = (req, res) => {
  Notification.find()
    .then(notifications => {
      res.send(notifications);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving notifications."
      });
    });
};

// Find a single notification with a notificationId
exports.findOne = (req, res) => {
  Notification.findById(req.params.notificationId)
    .then(notification => {
      if (!notification) {
        return res.status(404).send({
          message: "Notification not found with id " + req.params.notificationId
        });
      }
      res.send(notification);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "Notification not found with id " + req.params.notificationId
        });
      }
      return res.status(500).send({
        message:
          "Error retrieving notification with id " + req.params.notificationId
      });
    });
};

// Update a notification identified by the notificationId in the request
exports.update = (req, res) => {
  // Validate Request
  if (!req.body) {
    return res.status(400).send({
      message: "Notification content can not be empty"
    });
  }

  // Find notification and update it with the request body
  Notification.findByIdAndUpdate(
    req.params.notificationId,
    {
      for: req.body.for || "*",
      payload: req.body.payload
    },
    { new: true }
  )
    .then(notification => {
      if (!notification) {
        return res.status(404).send({
          message: "Notification not found with id " + req.params.notificationId
        });
      }
      res.send(notification);
    })
    .catch(err => {
      if (err.kind === "ObjectId") {
        return res.status(404).send({
          message: "Notification not found with id " + req.params.notificationId
        });
      }
      return res.status(500).send({
        message:
          "Error updating notification with id " + req.params.notificationId
      });
    });
};

// Delete a notification with the specified notificationId in the request
exports.delete = (req, res) => {
  Notification.findByIdAndRemove(req.params.notificationId)
    .then(notification => {
      if (!notification) {
        return res.status(404).send({
          message: "Notification not found with id " + req.params.notificationId
        });
      }
      res.send({ message: "Notification deleted successfully!" });
    })
    .catch(err => {
      if (err.kind === "ObjectId" || err.name === "NotFound") {
        return res.status(404).send({
          message: "Notification not found with id " + req.params.notificationId
        });
      }
      return res.status(500).send({
        message:
          "Could not delete notification with id " + req.params.notificationId
      });
    });
};
