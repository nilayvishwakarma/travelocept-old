﻿using Android.App;
using Android.Widget;
using Travelocept.Droid;
using Travelocept.Services.Native;

[assembly: Xamarin.Forms.Dependency(typeof(ToastAndroid))]
namespace Travelocept.Droid
{
    public class ToastAndroid : IToastMessage
    {
        public void LongAlert(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Long).Show();
        }

        public void ShortAlert(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Short).Show();
        }
    }
}