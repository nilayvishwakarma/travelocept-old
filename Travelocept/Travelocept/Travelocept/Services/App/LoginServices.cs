﻿using System;
using System.Threading.Tasks;
using Travelocept.Base;
using Travelocept.Models;
using Travelocept.Services.Utility;

namespace Travelocept.Services.App
{
    public class SigninServices
    {
        public BaseProvider Provider { get; private set; }

        public SigninServices(BaseProvider provider)
        {
            Provider = provider;
        }

        public async Task<Tuple<LoginResult, Login>> SignIn(string username, string password)
        {
            try
            {
                var user = await Provider.FirstOrDefault<Login>("logins", l => l.MobileNumber == username);
                if (user != default(Login))
                {
                    if (Crypto.VerifyPassword(password, user.Password))
                    {
                        return new Tuple<LoginResult, Login>(
                            LoginResult.LoginSuccessful,
                            user);
                    }
                    else
                    {
                        return new Tuple<LoginResult, Login>(
                            LoginResult.PasswordNotValid,
                            null);
                    }
                }
                else
                {
                    return new Tuple<LoginResult, Login>(
                            LoginResult.UserNotFound,
                            null);
                }
            }
            catch (TimeoutException)
            {
                return new Tuple<LoginResult, Login>(
                            LoginResult.CannotConnectToServer,
                            null);
            }
            catch (Exception)
            {
                return new Tuple<LoginResult, Login>(
                            LoginResult.Other,
                            null);
            }
        }

        public bool IsAllowed(LoginRole targetRole, LoginRole currentRole)
        {
            switch (currentRole)
            {
                case LoginRole.SuperAdmin:
                    return true;
                case LoginRole.Admin:
                    return targetRole != LoginRole.SuperAdmin;
                case LoginRole.Driver:
                    return targetRole == LoginRole.Driver;
                case LoginRole.User:
                    return targetRole == LoginRole.User;
            }
            return false;
        }

        public async Task<Tuple<SignupResult, Login>> SignUp(string username, string password, LoginRole role)
        {
            try
            {
                var hashPass = Crypto.HashPassword(password);
                var user = await Provider.FirstOrDefault<Login>("login", l => l.MobileNumber == username);
                if (user == null || user == default(Login))
                {
                    await Provider.Insert<Login>("logins", new Login
                    {
                        Enabled = true,
                        Role = role,
                        MobileNumber = username,
                        Password = hashPass
                    });
                    return new Tuple<SignupResult, Login>(
                            SignupResult.SignupSuccessful,
                            user);
                }
                else
                {
                    return new Tuple<SignupResult, Login>(
                            SignupResult.UserAlreadyExists,
                            null);
                }
            }
            catch (TimeoutException)
            {
                return new Tuple<SignupResult, Login>(
                            SignupResult.CannotConnectToServer,
                            null);
            }
            catch (Exception)
            {
                return new Tuple<SignupResult, Login>(
                            SignupResult.Other,
                            null);
            }
        }
    }

    public enum LoginResult
    {
        LoginSuccessful,
        UserNotFound,
        PasswordNotValid,
        RoleNotValid,
        CannotConnectToServer,
        Other
    }

    public enum SignupResult
    {
        SignupSuccessful,
        UserAlreadyExists,
        CannotConnectToServer,
        Other
    }
}
