﻿using System.Threading.Tasks;
using Travelocept.Base;
using Travelocept.Models;

namespace Travelocept.Services.App
{
    public class CrudServices
    {
        public BaseProvider Provider { get; private set; }

        public CrudServices(BaseProvider provider)
        {
            Provider = provider;
        }

        public async Task<Cab> AddCab(Cab item)
        {
            return await Provider.Insert<Cab>("cabs", item);
        }

        public async Task<Driver> AddDriver(Driver item)
        {
            return await Provider.Insert<Driver>("drivers", item);
        }

        public async Task<Trip> AddTrip(Trip item)
        {
            return await Provider.Insert<Trip>("trips", item);
        }

        public async Task<Login> AddUser(Login item)
        {
            item.Password = Utility.Crypto.HashPassword(item.Password);
            return await Provider.Insert<Login>("logins", item);
        }

        public async Task<Cab> EditCab(Cab item)
        {
            var cab = await Provider.UpdateOne<Cab>("cabs", i => i.Id == item.Id, item);
            await Provider.Update<Trip>("trips", t => t.Cab.Id == cab.Id, t => t.Cab, cab);
            return cab;
        }

        public async Task<Driver> EditDriver(Driver item)
        {
            var driver = await Provider.UpdateOne<Driver>("drivers", i => i.Id == item.Id, item);
            await Provider.Update<Trip>("trips", t => t.Cab.Id == driver.Id, t => t.Driver, driver);
            return driver;
        }

        public async Task<Trip> EditTrip(Trip item)
        {
            return await Provider.UpdateOne<Trip>("trips", i => i.Id == item.Id, item);
        }

        public async Task<Login> EditUser(Login item)
        {
            return await Provider.UpdateOne<Login>("logins", i => i.Id == item.Id, item);
        }

    }
}
