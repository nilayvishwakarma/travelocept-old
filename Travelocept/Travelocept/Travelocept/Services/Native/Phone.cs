﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Travelocept.Services.Native
{
    /// <summary>
    /// Enables an application to open a phone number in the dialer.
    /// </summary>
    public class Phone
    {
        public void Call(string number)
        {
            try
            {
                PhoneDialer.Open(number);
            }
            catch
            {
                // Other error has occurred.
            }
        }
    }
}
