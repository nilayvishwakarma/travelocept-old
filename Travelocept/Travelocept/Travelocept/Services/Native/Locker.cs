﻿using Plugin.SecureStorage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Travelocept.Services.Native
{
    /// <summary>
    /// This API is intended to store small amounts of text.
    /// Performance may be slow if you try to use it to store
    /// large amounts of text.
    /// </summary>
    public class Locker
    {
        public static async Task Store(string key, string value)
        {
            try
            {
                await Task.FromResult<bool>(CrossSecureStorage.Current.SetValue(key, value));
            }
            catch
            {
                try
                {
                    await SecureStorage.SetAsync(key, value);
                }
                catch
                {
                }
            }
        }

        public static async Task<string> Get(string key)
        {
            try
            {
                return await Task.FromResult<string>(CrossSecureStorage.Current.GetValue(key));
            }
            catch
            {
                try
                {
                    return await SecureStorage.GetAsync(key);
                }
                catch
                {
                    return null;
                }
            }
        }

        public static void Clear(string key)
        {
            try
            {

                if (key != null)
                {
                    CrossSecureStorage.Current.DeleteKey(key);
                }
            }
            catch
            {
                try
                {
                    SecureStorage.Remove(key);
                }
                catch
                {

                }
            }
        }
    }
}
