﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Travelocept.Services.Native
{
    /// <summary>
    /// The Email class enables an application to open the 
    /// default email application with a specified information 
    /// including subject, body, and recipients (TO, CC, BCC).
    /// </summary>
    public class Email
    {
        public async Task SendEmail(
            string subject,
            string body,
            List<string> recipients,
            List<string> ccRecipients = null)
        {
            ccRecipients = ccRecipients ?? new List<string>();
            try
            {
                var message = new EmailMessage
                {
                    Subject = subject,
                    Body = body,
                    To = recipients,
                    Cc = ccRecipients,
                    //Bcc = bccRecipients
                };
                await Xamarin.Essentials.Email.ComposeAsync(message);
            }
            catch
            {

            }
        }
    }
}
