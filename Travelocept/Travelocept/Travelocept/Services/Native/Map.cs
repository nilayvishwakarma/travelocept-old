﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Travelocept.Services.Native
{
    /// <summary>
    /// Retrieve the device's current geolocation coordinates
    ///  - Coarse and Fine Location permissions are required and 
    ///     must be configured in the Android project
    ///  - if your app targets Android 5.0 (API level 21) or higher,
    ///     you must declare that your app uses the hardware features
    ///     in the manifest file
    ///  - Permissions:
    ///     ACCESS_COARSE_LOCATION
    ///     ACCESS_FINE_LOCATION
    /// </summary>
    public class Map
    {
        public static async Task<Location> GetCurrentLocation()
        {
            var location = await Geolocation.GetLastKnownLocationAsync();
            return location;
        }

        public static async Task<Location> GetCoordinatesofAddress(string address)
        {
            var locations = await Geocoding.GetLocationsAsync(address);
            return locations.FirstOrDefault();
        }
    }
}
