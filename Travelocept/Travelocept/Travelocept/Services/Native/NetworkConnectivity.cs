﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Travelocept.Services.Native
{
    /// <summary>
    /// It is important to note that it is possible that 
    /// Internet is reported by NetworkAccess but full access 
    /// to the web is not available. Due to how connectivity 
    /// works on each platform it can only guarantee that a 
    /// connection is available. For instance the device may 
    /// be connected to a Wi-Fi network, but the router is 
    /// disconnected from the internet. In this instance Internet 
    /// may be reported, but an active connection is not available.
    /// - Permissions
    ///     ACCESS_NETWORK_STATE
    /// </summary>
    public class NetworkConnectivity
    {
        public static async Task<bool> IsOnline()
        {
            var current = Connectivity.NetworkAccess;
            if (current == NetworkAccess.Internet)
            {
                return await Task.FromResult(true);
            }
            else
            {
                return await Task.FromResult(false);
            }
        }
    }
}
