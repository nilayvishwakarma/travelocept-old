﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Travelocept.Services.Native
{
    public class Toaster
    {
        public static void LongAlert(string message)
        {
            DependencyService.Get<IToastMessage>().LongAlert(message);
        }

        public static void ShortAlert(string message)
        {
            DependencyService.Get<IToastMessage>().ShortAlert(message);
        }
    }

    public interface IToastMessage
    {
        void LongAlert(string message);
        void ShortAlert(string message);
    }
}
