﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Quobject.SocketIoClientDotNet.Client;

namespace Travelocept.Services.Native
{
    /// <summary>
    /// Real-time bidirectional event-based communication
    /// 
    /// Error Handling :
    ///   Connect − When the client successfully connects.
    ///   Connecting − When the client is in the process of connecting.
    ///   Disconnect − When the client is disconnected.
    ///   Connect_failed − When the connection to the server fails.
    ///   Error − An error event is sent from the server.
    ///   Message − When the server sends a message using the send function.
    ///   Reconnect − When reconnection to the server is successful.
    ///   Reconnecting − When the client is in the process of connecting.
    ///   Reconnect_failed − When the reconnection attempt fails.
    /// </summary>
    public class NotificationHub
    {
        public event EventHandler<NotificationEventArgs> Recieved;
        public event EventHandler<NotificationEventArgs> Connected;
        public event EventHandler<NotificationEventArgs> Disconnected;
        public string Username { get; private set; }
        Socket socket = null;
        public bool IsReady = false;
        public NotificationHub(string serverUrl, string username)
        {
            Username = username;

            IO.Options options = new IO.Options()
            {
                Reconnection = true,
                ReconnectionDelay = 1000,
                ReconnectionDelayMax = 5000,
                ReconnectionAttempts = int.MaxValue
            };
            socket = IO.Socket(serverUrl, options);

            socket.On(Socket.EVENT_DISCONNECT, () =>
            {
                RaiseDisconnected();
            });

            socket.On(Socket.EVENT_CONNECT, () =>
            {
                RaiseConnected();
                socket.Emit("join", Username);
                IsReady = true;
            });

            socket.On("new_msg", (data) =>
            {
                RaiseRecieved(data);
            });

            socket.On("message", (data) =>
            {
                RaiseRecieved(data);
            });
        }

        public void SendNotification(string targetMobile, string message)
        {
            if (IsReady)
            {
                if (targetMobile == null || targetMobile == "*")
                {

                }
                else
                {

                }
            }
            else
            {
                throw new NotImplementedException();
            }
            
        }

        private void RaiseRecieved(object message = null)
        {
            Recieved?.Invoke(this, new NotificationEventArgs(message.ToString()));
        }

        private void RaiseConnected()
        {
            Connected?.Invoke(this, NotificationEventArgs.Empty);
        }

        private void RaiseDisconnected()
        {
            Disconnected?.Invoke(this, NotificationEventArgs.Empty);
        }
    }

    public class NotificationEventArgs : EventArgs
    {
        public string Message { get; private set; }
        public NotificationType Type { get; private set; }
        public NotificationEventArgs(
            string message = null,
            NotificationType type = NotificationType.Broadcast)
        {
            Message = message ?? null;
            Type = type;
        }

        public new static NotificationEventArgs Empty
        {
            get
            {
                return new NotificationEventArgs();
            }
        }
    }

    public enum NotificationType
    {
        Broadcast,
        Private
    }


}
