﻿using Plugin.LocalNotifications;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Travelocept.Services.Native
{
    public class Notification
    {
        public static void Show(string title, string body, int id, DateTime notifyTime)
        {
            CrossLocalNotifications.Current.Show(
                title,
                body,
                id,
                notifyTime
                );
        }

        public static void Cancel(int id)
        {
            CrossLocalNotifications.Current.Cancel(id);
        }
    }
}
