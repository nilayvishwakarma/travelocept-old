﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq.Expressions;
using Travelocept.Base;
using System.Threading.Tasks;

namespace Travelocept.Services.Data
{
    public class MongoProvider : BaseProvider
    {
        public static MongoProvider Provider => new MongoProvider(
            "mongodb://root:password123@ds036178.mlab.com:36178/teleporter?connect=replicaSet"
            );
        MongoClient client = null;
        IMongoDatabase db = null;
        MongoCollectionSettings defaultCollectionSetting = new MongoCollectionSettings { ReadPreference = ReadPreference.Nearest };

        private MongoProvider(string connectionString)
        {
            // MongoLab doesn't like new MongoSettings(new MongoUri("..."))
            // https://stackoverflow.com/questions/31314245/a-timeout-occured-after-30000ms-selecting-a-server-using-compositeserverselector
            //var settings = MongoClientSettings.FromUrl(new MongoUrl(connectionString));
            //settings.SslSettings = new SslSettings
            //{
            //    EnabledSslProtocols = System.Security.Authentication.SslProtocols.Tls12
            //};
            client = new MongoClient(connectionString);
            db = client.GetDatabase("teleporter");
        }

        public async Task<T> FirstOrDefault<T>(
            string collectionName,
            Expression<Func<T, bool>> filter) where T : BaseModel
        {
            return await ((await db
                .GetCollection<T>(collectionName, defaultCollectionSetting)
                .FindAsync<T>(filter))
                .FirstOrDefaultAsync<T>());
        }


        public async Task<T> First<T>(
            string collectionName,
            Expression<Func<T, bool>> filter) where T : BaseModel
        {
            return await ((await db
                .GetCollection<T>(collectionName, defaultCollectionSetting)
                .FindAsync<T>(filter))
                .FirstAsync<T>());
        }

        public async Task<ObservableCollection<T>> Where<T>(
            string collectionName,
            Expression<Func<T, bool>> filter) where T : BaseModel
        {
            return
                new ObservableCollection<T>(await ((await db
                .GetCollection<T>(collectionName, defaultCollectionSetting)
                .FindAsync<T>(filter))
                .ToListAsync<T>()));
        }

        public async Task<ObservableCollection<T>> All<T>(
            string collectionName) where T : BaseModel
        {
            return
                new ObservableCollection<T>(await ((await db
                .GetCollection<T>(collectionName, defaultCollectionSetting)
                .FindAsync(Builders<T>.Filter.Empty))
                .ToListAsync<T>()));
        }

        public async Task<bool> Any<T>(
            string collectionName,
            Expression<Func<T, bool>> filter) where T : BaseModel
        {
            return
                (await (await db
                .GetCollection<T>(collectionName, defaultCollectionSetting)
                .FindAsync<T>(filter))
                .AnyAsync<T>());
        }

        public async Task<T> Insert<T>(
            string collectionName,
            T item) where T : BaseModel
        {
            item.Id = new Guid();
            item.Enabled = true;
            item.CreatedTime = item.ModifiedTime = DateTime.Now;
            await db
                .GetCollection<T>(collectionName, defaultCollectionSetting)
                .InsertOneAsync(item);
            return item;
        }

        public async Task<IEnumerable<T>> InsertRange<T>(
            string collectionName,
            List<T> items) where T : BaseModel
        {
            foreach (var item in items)
            {
                item.Id = new Guid();
                item.Enabled = true;
                item.CreatedTime = item.ModifiedTime = DateTime.Now;
            }
            await db
                .GetCollection<T>(collectionName, defaultCollectionSetting)
                .InsertManyAsync(items);
            return items;
        }

        public async Task<T> UpdateOne<T>(
            string collectionName,
            Expression<Func<T, bool>> filter,
            T item
            ) where T : BaseModel
        {
            item.ModifiedTime = DateTime.Now;
            await db
                .GetCollection<T>(collectionName, defaultCollectionSetting)
                .FindOneAndReplaceAsync<T>(filter, item);
            return item;
        }

        public async Task Update<T>(
            string collectionName,
            Expression<Func<T, bool>> filter,
            Expression<Func<T, object>> updateExpr, object value
            ) where T : BaseModel
        {
            await db
                .GetCollection<T>(collectionName, defaultCollectionSetting)
                .UpdateManyAsync<T>(
                    filter,
                    Builders<T>
                        .Update
                        .Set(updateExpr, value)
                        .Set(t => t.ModifiedTime, DateTime.Now));
        }

        public async Task Delete<T>(
            string collectionName,
            Expression<Func<T, bool>> filter
            ) where T : BaseModel
        {
            await db
                .GetCollection<T>(collectionName, defaultCollectionSetting)
                .DeleteManyAsync<T>(filter);
        }
    }
}
