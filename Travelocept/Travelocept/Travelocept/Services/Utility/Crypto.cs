﻿using System;

namespace Travelocept.Services.Utility
{
    public class Crypto
    {
        /// <summary>
        /// https://github.com/BcryptNet/bcrypt.net
        /// </summary>
        /// <param name="inputPassword"></param>
        /// <returns></returns>
        public static string HashPassword(string inputPassword)
        {
            return BCrypt.Net.BCrypt.EnhancedHashPassword(inputPassword);
        }

        /// <summary>
        /// https://github.com/BcryptNet/bcrypt.net
        /// </summary>
        /// <param name="inputPassword"></param>
        /// <param name="hashedPassword"></param>
        /// <returns></returns>
        public static bool VerifyPassword(string inputPassword, string hashedPassword)
        {
            try
            {
                return BCrypt.Net.BCrypt.EnhancedVerify(
                    inputPassword,
                    hashedPassword);
            }
            catch
            {
                return false;
            }

        }

        public static string GenerateOTP(
            int iOTPLength = 4)
        {
            string[] saAllowedCharacters = 
            {
                "1", "2", "3", "4", "5",
                "6", "7", "8", "9", "0"
            };
            string sOTP = String.Empty;
            string sTempChars = String.Empty;
            Random rand = new Random();
            for (int i = 0; i < iOTPLength; i++)
            {
                int p = rand.Next(0, saAllowedCharacters.Length);
                sTempChars = saAllowedCharacters[rand.Next(0, saAllowedCharacters.Length)];
                sOTP += sTempChars;

            }
            return sOTP;
        }
    }
}
