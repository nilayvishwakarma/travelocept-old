﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Travelocept.Base;
using Travelocept.Models;
using Travelocept.Services.App;
using Travelocept.Services.Native;
using Travelocept.Views;
using Xamarin.Forms;

namespace Travelocept.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        public LoginViewModel(
            INavigation navigation,
            BaseProvider provider) : base(navigation, provider)
        {

        }
        bool loginStarted = false;
        string username = string.Empty;
        string password = string.Empty;

        public bool LoginStarted
        {
            get { return loginStarted; }
            set { SetProperty(ref loginStarted, value); }
        }
        public string Username
        {
            get { return username; }
            set { SetProperty(ref username, value); }
        }
        public string Password
        {
            get { return password; }
            set { SetProperty(ref password, value); }
        }
        public Command LoginUser
        {
            get
            {
                return new Command(async () =>
                {
                    await _LoginUser();
                },
                () =>
                {
                    return !LoginStarted;
                });
            }
        }

        public Command SignupUser
        {
            get
            {
                return new Command(async () =>
                {
                    await _CreateUser();
                },
                () =>
                {
                    return !LoginStarted;
                });
            }
        }

        private async Task _LoginUser()
        {
            LoginStarted = true;
            try
            {
                var loginResult = await LoginService.SignIn(username, password);
                switch (loginResult.Item1)
                {
                    case LoginResult.LoginSuccessful:
                        {
                            await Locker.Store("travelocept_username", username);
                            await Locker.Store("travelocept_password", password);
                            await Task.Run(() =>
                            {
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    Application.Current.MainPage = new MainPage(loginResult.Item2.Role);
                                });
                            });
                        }
                        break;
                    case LoginResult.UserNotFound:
                        Toaster.LongAlert("Cannot find this user!!");
                        break;
                    case LoginResult.PasswordNotValid:
                        Toaster.LongAlert("Invalid password!!");
                        break;
                    case LoginResult.RoleNotValid:
                        Toaster.LongAlert("Not a valid Role!!");
                        break;
                    case LoginResult.CannotConnectToServer:
                        Toaster.LongAlert("Cannot connect to server!!");
                        break;
                    default:
                        Toaster.LongAlert("Cannot login, server threw an error!!");
                        break;

                }
            }
            finally
            {
                LoginStarted = false;
            }
        }

        private async Task _CreateUser()
        {
            LoginStarted = true;
            try
            {
                var signupResult = await LoginService.SignUp(username, password, LoginRole.Admin);
                if (signupResult.Item1 == SignupResult.SignupSuccessful)
                {
                    await Locker.Store("travelocept_username", username);
                    await Locker.Store("travelocept_password", password);
                }
                else
                {
                    Toaster.LongAlert("Cannot create user!!");
                }
            }
            finally
            {
                LoginStarted = false;
            }
            
        }
    }
}
