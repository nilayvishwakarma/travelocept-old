﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Travelocept.Base;
using Travelocept.Controls.Dialogs;
using Travelocept.Extensions;
using Travelocept.Models;
using Xamarin.Forms;

namespace Travelocept.ViewModels.Admin
{
    public class TripsViewModel : BaseViewModel
    {

        public TripsViewModel(
            INavigation navigation,
            BaseProvider provider) : base(navigation, provider)
        {
            this._cabs = new ObservableCollection<Cab>();
            this._drivers = new ObservableCollection<Driver>();
            this._trips = new ObservableCollection<Trip>();
            LoadAllData().Wait();
        }

        

        private bool _isRefreshingTrip = false;
        public bool IsRefreshingTrip
        {
            get { return _isRefreshingTrip; }
            set
            {
                SetProperty(ref _isRefreshingTrip, value);
            }
        }
        public Command RefreshTripCommand
        {
            get
            {
                return new Command(async () =>
                {
                    // Don't reload if we're already doing so...
                    if (this.IsRefreshingTrip)
                    {
                        return;
                    }
                    IsRefreshingTrip = true;
                    if (this.RefreshTripCommand.CanExecute(default(object)))
                    {
                        this.RefreshTripCommand.ChangeCanExecute();
                    }
                    try
                    {
                        Drivers = await Provider.All<Driver>("drivers");
                        Cabs = await Provider.All<Cab>("cabs");
                        Trips = await Provider.All<Trip>("trips");
                    }
                    finally
                    {
                        IsRefreshingTrip = false;
                        if (!this.RefreshTripCommand.CanExecute(default(object)))
                        {
                            this.RefreshTripCommand.ChangeCanExecute();
                        }
                    }
                });
            }
        }

        private ObservableCollection<Cab> _cabs;
        private ObservableCollection<Driver> _drivers;
        private ObservableCollection<Trip> _trips;

        public ObservableCollection<Trip> Trips
        {
            get
            {
                return _trips;
            }
            set
            {
                SetProperty(ref _trips, value);
            }
        }

        public ObservableCollection<Cab> Cabs
        {
            get
            {
                return _cabs;
            }
            set
            {
                SetProperty(ref _cabs, value);
            }
        }

        public ObservableCollection<Driver> Drivers
        {
            get
            {
                return _drivers;
            }
            set
            {
                SetProperty(ref _drivers, value);
            }
        }



        public async Task LoadAllData()
        {
            Drivers = await Provider.All<Driver>("drivers");
            Cabs = await Provider.All<Cab>("cabs");
            Trips = await Provider.All<Trip>("trips");

            MessagingCenter.Subscribe<ModifyTrip, Trip>(this, "AddTrip", async (obj, item) =>
            {
                this.Trips.Add(await CrudService.AddTrip(item as Trip));
            });
            MessagingCenter.Subscribe<ModifyTrip, Trip>(this, "EditTrip", async (obj, item) =>
            {
                this.Trips.Replace(await CrudService.EditTrip(item as Trip));
            });
        }
    }
}
