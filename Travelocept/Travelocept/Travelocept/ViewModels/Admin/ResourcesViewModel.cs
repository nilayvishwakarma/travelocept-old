﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using Travelocept.Base;
using Travelocept.Controls.Dialogs;
using Travelocept.Extensions;
using Travelocept.Models;
using Xamarin.Forms;

namespace Travelocept.ViewModels.Admin
{
    public class ResourcesViewModel : BaseViewModel
    {
        

        public ResourcesViewModel(
            INavigation navigation,
            BaseProvider provider) : base(navigation, provider)
        {
            this._cabs = new ObservableCollection<Cab>();
            this._drivers = new ObservableCollection<Driver>();
            LoadAllData().Wait();
        }

        private ObservableCollection<Cab> _cabs;
        private ObservableCollection<Driver> _drivers;
        public ObservableCollection<Cab> Cabs
        {
            get
            {
                return _cabs;
            }
            set
            {
                SetProperty(ref _cabs, value);
            }
        }

        public ObservableCollection<Driver> Drivers
        {
            get
            {
                return _drivers;
            }
            set
            {
                SetProperty(ref _drivers, value);
            }
        }

        private bool _isRefreshingCab = false;
        public bool IsRefreshingCab
        {
            get { return _isRefreshingCab; }
            set
            {
                SetProperty(ref _isRefreshingCab, value);
            }
        }
        public Command RefreshCabCommand
        {
            get
            {
                return new Command(async () =>
                {
                    // Don't reload if we're already doing so...
                    if (this.IsRefreshingCab)
                    {
                        return;
                    }
                    IsRefreshingCab = true;
                    if (this.RefreshCabCommand.CanExecute(default(object)))
                    {
                        this.RefreshCabCommand.ChangeCanExecute();
                    }
                    try
                    {
                        Cabs = await Provider.All<Cab>("cabs");
                    }
                    finally
                    {
                        IsRefreshingCab = false;
                        if (!this.RefreshCabCommand.CanExecute(default(object)))
                        {
                            this.RefreshCabCommand.ChangeCanExecute();
                        }
                    }
                });
            }
        }

        private bool _isRefreshingDriver = false;
        public bool IsRefreshingDriver
        {
            get { return _isRefreshingDriver; }
            set
            {
                SetProperty(ref _isRefreshingDriver, value);
            }
        }
        public Command RefreshDriverCommand
        {
            get
            {
                return new Command(async () =>
                {
                    // Don't reload if we're already doing so...
                    if (this.IsRefreshingDriver)
                    {
                        return;
                    }
                    IsRefreshingDriver = true;
                    if (this.RefreshDriverCommand.CanExecute(default(object)))
                    {
                        this.RefreshDriverCommand.ChangeCanExecute();
                    }
                    try
                    {
                        Drivers = await Provider.All<Driver>("drivers");
                    }
                    finally
                    {
                        IsRefreshingDriver = false;
                        if (!this.RefreshDriverCommand.CanExecute(default(object)))
                        {
                            this.RefreshDriverCommand.ChangeCanExecute();
                        }
                    }
                });
            }
        }

        public async Task LoadAllData()
        {
            Drivers = await Provider.All<Driver>("drivers");
            Cabs = await Provider.All<Cab>("cabs");

            MessagingCenter.Subscribe<ModifyDriver, Driver>(this, "AddDriver", async (obj, item) =>
            {
                this.Drivers.Add(await CrudService.AddDriver(item as Driver));
            });
            MessagingCenter.Subscribe<ModifyDriver, Driver>(this, "EditDriver", async (obj, item) =>
            {
                this.Drivers.Replace(await CrudService.EditDriver(item as Driver));
            });

            MessagingCenter.Subscribe<ModifyCab, Cab>(this, "AddCab", async (obj, item) =>
            {
                this.Cabs.Add(await CrudService.AddCab(item as Cab));
            });
            MessagingCenter.Subscribe<ModifyCab, Cab>(this, "EditCab", async (obj, item) =>
            {
                this.Cabs.Replace(await CrudService.EditCab(item as Cab));
            });
        }
    }
}
