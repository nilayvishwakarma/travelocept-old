﻿using System;
using System.Collections.Generic;
using System.Text;
using Travelocept.Base;
using Xamarin.Forms;

namespace Travelocept.ViewModels.User
{
    public class SettingsViewModel : BaseViewModel
    {
        public SettingsViewModel(INavigation navigation, BaseProvider provider)
            : base(navigation, provider)
        {
        }
    }
}
