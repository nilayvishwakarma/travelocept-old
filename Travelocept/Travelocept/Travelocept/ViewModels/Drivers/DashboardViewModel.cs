﻿using System;
using System.Collections.Generic;
using System.Text;
using Travelocept.Base;
using Xamarin.Forms;

namespace Travelocept.ViewModels.Drivers
{
    public class DashboardViewModel : BaseViewModel
    {
        public DashboardViewModel(INavigation navigation, BaseProvider provider) 
            : base(navigation, provider)
        {
        }
    }
}
