﻿using System;
using System.Collections.Generic;
using System.Text;
using Travelocept.Base;
using Xamarin.Forms;

namespace Travelocept.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        public AboutViewModel(INavigation navigation, BaseProvider provider)
            : base(navigation, provider)
        {
        }
    }
}
