﻿using System;
using System.Collections.Generic;
using System.Text;
using Travelocept.Base;

namespace Travelocept.Models
{
    public class Cab : BaseModel
    {
        public string Cabname { get; set; }
        public string LicenseNumber { get; set; }
    }

    public class CabViewModel : ModelViewModel<Cab>
    {
        public CabViewModel(Cab cab) : base(cab)
        {
            Model = cab;
        }

        public override Cab Model { get; set; }

        public string Cabname
        {
            get
            {
                return Model.Cabname;
            }
            set
            {
                if (Model.Cabname != value)
                {
                    Model.Cabname = value;
                    OnPropertyChanged();
                }
            }
        }
        public string LicenseNumber
        {
            get
            {
                return Model.LicenseNumber;
            }
            set
            {
                if (Model.LicenseNumber != value)
                {
                    Model.LicenseNumber = value;
                    OnPropertyChanged();
                }
            }
        }
    }
}
