﻿using System;
using System.Collections.Generic;
using System.Text;
using Travelocept.Base;

namespace Travelocept.Models
{
    public class Login : BaseModel
    {
        public string MobileNumber { get; set; }
        public LoginRole Role { get; set; }
        public string Password { get; set; }
    }

    public enum LoginRole
    {
        SuperAdmin = 0,
        Admin = 1,
        Driver = 2,
        User = 3
    }

    public class LoginUserViewModel : ModelViewModel<Login>
    {
        public LoginUserViewModel(Login login) : base(login)
        {
            Model = login;
        }

        public override Login Model { get; set; }

        public string MobileNumber
        {
            get
            {
                return Model.MobileNumber;
            }
            set
            {
                if (Model.MobileNumber != value)
                {
                    Model.MobileNumber = value;
                    OnPropertyChanged();
                }
            }
        }
        public string Password
        {
            get
            {
                return Model.Password;
            }
            set
            {
                if (Model.Password != value)
                {
                    Model.Password = value;
                    OnPropertyChanged();
                }
            }
        }

        public LoginRole Role
        {
            get
            {
                return Model.Role;
            }
            set
            {
                if (Model.Role != value)
                {
                    Model.Role = value;
                    OnPropertyChanged();
                }
            }
        }
    }
}
