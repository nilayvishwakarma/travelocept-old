﻿using System;
using System.Collections.Generic;
using System.Text;
using Travelocept.Base;

namespace Travelocept.Models
{
    public class Driver : BaseModel
    {
        public string Name { get; set; }
        public string MobileNumber { get; set; }
    }

    public class DriverViewModel : ModelViewModel<Driver>
    {
        public DriverViewModel(Driver driver) : base(driver)
        {
            Model = driver;
        }

        public override Driver Model { get; set; }

        public string Name
        {
            get
            {
                return Model.Name;
            }
            set
            {
                if (Model.Name != value)
                {
                    Model.Name = value;
                    OnPropertyChanged();
                }
            }
        }
        public string MobileNumber
        {
            get
            {
                return Model.MobileNumber;
            }
            set
            {
                if (Model.MobileNumber != value)
                {
                    Model.MobileNumber = value;
                    OnPropertyChanged();
                }
            }
        }
    }
}
