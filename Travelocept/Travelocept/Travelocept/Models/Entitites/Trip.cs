﻿using System;
using System.Collections.Generic;
using System.Text;
using Travelocept.Base;

namespace Travelocept.Models
{
    public class Trip : BaseModel
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string StartAddress { get; set; }
        public string EndAddress { get; set; }
        public string StartKm { get; set; }
        public string EndKm { get; set; }
        public string OTP { get; set; }
        public TripStatus Status { get; set; }
        public Driver Driver { get; set; }
        public Cab Cab { get; set; }
    }

    public enum TripStatus
    {
        Created,
        Assigned,
        Started,
        Abandoned,
        Finished
    }

    public class TripViewModel : ModelViewModel<Trip>
    {
        public TripViewModel(Trip trip) : base(trip)
        {
            Model = trip;
        }

        public override Trip Model { get; set; }

        public DateTime StartTime
        {
            get
            {
                return Model.StartTime;
            }
            set
            {
                if (Model.StartTime != value)
                {
                    Model.StartTime = value;
                    OnPropertyChanged();
                }
            }
        }
        public DateTime EndTime
        {
            get
            {
                return Model.EndTime;
            }
            set
            {
                if (Model.EndTime != value)
                {
                    Model.EndTime = value;
                    OnPropertyChanged();
                }
            }
        }
        public string StartAddress
        {
            get
            {
                return Model.StartAddress;
            }
            set
            {
                if (Model.StartAddress != value)
                {
                    Model.StartAddress = value;
                    OnPropertyChanged();
                }
            }
        }
        public string EndAddress
        {
            get
            {
                return Model.EndAddress;
            }
            set
            {
                if (Model.EndAddress != value)
                {
                    Model.EndAddress = value;
                    OnPropertyChanged();
                }
            }
        }
        public string StartKm
        {
            get
            {
                return Model.StartKm;
            }
            set
            {
                if (Model.StartKm != value)
                {
                    Model.StartKm = value;
                    OnPropertyChanged();
                }
            }
        }
        public string EndKm
        {
            get
            {
                return Model.EndKm;
            }
            set
            {
                if (Model.EndKm != value)
                {
                    Model.EndKm = value;
                    OnPropertyChanged();
                }
            }
        }
        public string OTP
        {
            get
            {
                return Model.OTP;
            }
            set
            {
                if (Model.OTP != value)
                {
                    Model.OTP = value;
                    OnPropertyChanged();
                }
            }
        }
        public TripStatus Status
        {
            get
            {
                return Model.Status;
            }
            set
            {
                if (Model.Status != value)
                {
                    Model.Status = value;
                    OnPropertyChanged();
                }
            }
        }
        List<Driver> drivers;
        public List<Driver> Drivers
        {
            get
            {
                return drivers;
            }
            set
            {
                if (drivers != value)
                {
                    drivers = value;
                    OnPropertyChanged();
                }
            }
        }

        public Driver Driver
        {
            get
            {
                return Model.Driver;
            }
            set
            {
                if (Model.Driver != value)
                {
                    Model.Driver = value;
                    OnPropertyChanged();
                }
            }
        }
        public Cab Cab
        {
            get
            {
                return Model.Cab;
            }
            set
            {
                if (Model.Cab != value)
                {
                    Model.Cab = value;
                    OnPropertyChanged();
                }
            }
        }

        List<Cab> cabs;
        public List<Cab> Cabs
        {
            get
            {
                return cabs;
            }
            set
            {
                if (cabs != value)
                {
                    cabs = value;
                    OnPropertyChanged();
                }
            }
        }
    }
}
