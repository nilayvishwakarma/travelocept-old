﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Travelocept.Models
{
    public enum MenuItemType
    {
        About,
        Settings,
        Dashboard,
        Resources,
        Trips
    }
    public class HomeMenuItem
    {
        public MenuItemType Id { get; set; }

        public string Title { get; set; }
    }
}
