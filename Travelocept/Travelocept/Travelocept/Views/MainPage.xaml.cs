﻿using Travelocept.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Travelocept.Views.Admin;

namespace Travelocept.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : MasterDetailPage
    {
        Dictionary<int, NavigationPage> MenuPages = new Dictionary<int, NavigationPage>();
        public LoginRole Role;
        public MainPage(LoginRole role)
        {
            Role = role;
            InitializeComponent();

            MasterBehavior = MasterBehavior.Popover;
            MenuPages.Add((int)MenuItemType.Dashboard, (NavigationPage)Detail);
        }

        public async Task NavigateFromMenu(int id)
        {
            if (!MenuPages.ContainsKey(id))
            {
                switch (Role)
                {
                    case LoginRole.SuperAdmin:
                    case LoginRole.Admin:
                        {
                            switch (id)
                            {
                                case (int)MenuItemType.About:
                                    MenuPages.Add(id, new NavigationPage(new AboutPage()));
                                    break;
                                case (int)MenuItemType.Dashboard:
                                    MenuPages.Add(id, new NavigationPage(new Admin.DashboardPage()));
                                    break;
                                case (int)MenuItemType.Resources:
                                    MenuPages.Add(id, new NavigationPage(new Admin.ResourcesPage()));
                                    break;
                                case (int)MenuItemType.Trips:
                                    MenuPages.Add(id, new NavigationPage(new Admin.TripsPage()));
                                    break;
                                case (int)MenuItemType.Settings:
                                    MenuPages.Add(id, new NavigationPage(new Admin.SettingsPage()));
                                    break;
                            }
                        }
                        break;
                    case LoginRole.Driver:
                        {
                            switch (id)
                            {
                                case (int)MenuItemType.About:
                                    MenuPages.Add(id, new NavigationPage(new AboutPage()));
                                    break;
                                case (int)MenuItemType.Dashboard:
                                    MenuPages.Add(id, new NavigationPage(new Drivers.DashboardPage()));
                                    break;
                                case (int)MenuItemType.Trips:
                                    MenuPages.Add(id, new NavigationPage(new Drivers.TripsPage()));
                                    break;
                                case (int)MenuItemType.Settings:
                                    MenuPages.Add(id, new NavigationPage(new Drivers.SettingsPage()));
                                    break;
                            }
                        }
                        break;
                    case LoginRole.User:
                        {
                            switch (id)
                            {
                                case (int)MenuItemType.About:
                                    MenuPages.Add(id, new NavigationPage(new AboutPage()));
                                    break;
                                case (int)MenuItemType.Dashboard:
                                    MenuPages.Add(id, new NavigationPage(new User.DashboardPage()));
                                    break;
                                case (int)MenuItemType.Trips:
                                    MenuPages.Add(id, new NavigationPage(new User.TripsPage()));
                                    break;
                                case (int)MenuItemType.Settings:
                                    MenuPages.Add(id, new NavigationPage(new User.SettingsPage()));
                                    break;
                            }
                        }
                        break;
                }
                
            }

            var newPage = MenuPages[id];

            if (newPage != null && Detail != newPage)
            {
                Detail = newPage;

                if (Device.RuntimePlatform == Device.Android)
                    await Task.Delay(100);

                IsPresented = false;
            }
        }
    }
}