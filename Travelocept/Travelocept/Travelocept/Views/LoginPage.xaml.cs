﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Travelocept.Services.Data;
using Travelocept.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Travelocept.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
        LoginViewModel vm;
        public LoginPage ()
		{
			InitializeComponent ();
            this.BindingContext = vm = new LoginViewModel(
                this.Navigation,
                MongoProvider.Provider);
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void txtUsername_Completed(object sender, EventArgs e)
        {
            txtPassword.Focus();
        }

        private void txtPassword_Completed(object sender, EventArgs e)
        {
            btnLogin.Command.Execute(null);
        }
    }
}