﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Travelocept.Services.Data;
using Travelocept.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Travelocept.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AboutPage : ContentPage
	{
        private AboutViewModel vm;

        public AboutPage ()
		{
			InitializeComponent ();
            this.BindingContext = vm = new AboutViewModel(Navigation, MongoProvider.Provider);
        }
	}
}