﻿using Travelocept.Models;
using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Travelocept.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuPage : ContentPage
    {
        MainPage RootPage { get => Application.Current.MainPage as MainPage; }
        List<HomeMenuItem> menuItems;
        public MenuPage()
        {
            InitializeComponent();

            switch (RootPage.Role)
            {
                case LoginRole.Admin:
                case LoginRole.SuperAdmin:
                    {
                        menuItems = new List<HomeMenuItem>
                        {
                            new HomeMenuItem {Id = MenuItemType.Dashboard, Title="Dashboard" },
                            new HomeMenuItem {Id = MenuItemType.Resources, Title="Resources" },
                            new HomeMenuItem {Id = MenuItemType.Trips, Title="Trips" },
                            new HomeMenuItem {Id = MenuItemType.Settings, Title="Settings" }
                        };
                        break;
                    }
                case LoginRole.Driver:
                    {
                        menuItems = new List<HomeMenuItem>
                        {
                            new HomeMenuItem {Id = MenuItemType.Dashboard, Title="Dashboard" },
                            new HomeMenuItem {Id = MenuItemType.Trips, Title="Trips" },
                            new HomeMenuItem {Id = MenuItemType.Settings, Title="Settings" }
                        };
                        break;
                    }
                case LoginRole.User:
                    {
                        menuItems = new List<HomeMenuItem>
                        {
                            new HomeMenuItem {Id = MenuItemType.Dashboard, Title="Dashboard" },
                            new HomeMenuItem {Id = MenuItemType.Trips, Title="Trips" },
                            new HomeMenuItem {Id = MenuItemType.Settings, Title="Settings" }
                        };
                        break;
                    }
            }

            menuItems.Add(
                new HomeMenuItem { Id = MenuItemType.About, Title = "About" }
                );
            

            ListViewMenu.ItemsSource = menuItems;

            ListViewMenu.SelectedItem = menuItems[0];
            ListViewMenu.ItemSelected += async (sender, e) =>
            {
                if (e.SelectedItem == null)
                    return;

                var id = (int)((HomeMenuItem)e.SelectedItem).Id;
                await RootPage.NavigateFromMenu(id);
            };
        }
    }
}