﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Travelocept.Controls.Dialogs;
using Travelocept.Models;
using Travelocept.Services.Data;
using Travelocept.ViewModels.Admin;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Travelocept.Views.Admin
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TripsPage : ContentPage
	{
        TripsViewModel vm;

        public TripsPage ()
		{
			InitializeComponent ();
            this.BindingContext = vm = new TripsViewModel(Navigation, MongoProvider.Provider);
        }

        private async void AddTrip_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new ModifyTrip(vm.Cabs.ToList(), vm.Drivers.ToList())));
        }

        private async void TripsListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (!(e.SelectedItem is Trip item))
                return;

            await Navigation.PushModalAsync(new NavigationPage(new ModifyTrip(vm.Cabs.ToList(), vm.Drivers.ToList(), item)));

            // Manually deselect item.
            (sender as ListView).SelectedItem = null;
        }
    }
}