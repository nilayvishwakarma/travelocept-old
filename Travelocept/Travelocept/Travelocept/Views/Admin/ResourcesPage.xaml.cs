﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Travelocept.Controls.Dialogs;
using Travelocept.Models;
using Travelocept.Services.Data;
using Travelocept.ViewModels.Admin;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Travelocept.Views.Admin
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ResourcesPage : TabbedPage
    {
        ResourcesViewModel vm;

        public ResourcesPage()
        {
            InitializeComponent();
            this.BindingContext = vm = new ResourcesViewModel(Navigation, MongoProvider.Provider);
        }

        private async void AddDriver_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new ModifyDriver()));
        }

        private async void AddCab_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new ModifyCab()));
        }

        private async void DriversListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (!(e.SelectedItem is Driver item))
                return;

            await Navigation.PushModalAsync(new NavigationPage(new ModifyDriver(item)));

            // Manually deselect item.
            (sender as ListView).SelectedItem = null;
        }

        private async void CabsListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (!(e.SelectedItem is Cab item))
                return;

            await Navigation.PushModalAsync(new NavigationPage(new ModifyCab(item)));

            // Manually deselect item.
            (sender as ListView).SelectedItem = null;
        }
    }
}