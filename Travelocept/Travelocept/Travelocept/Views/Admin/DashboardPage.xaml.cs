﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Travelocept.Services.Data;
using Travelocept.ViewModels.Admin;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Travelocept.Views.Admin
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class DashboardPage : ContentPage
	{
        DashboardViewModel vm;

        public DashboardPage()
		{
			InitializeComponent ();
            this.BindingContext = vm = new DashboardViewModel(Navigation, MongoProvider.Provider);
        }
	}
}