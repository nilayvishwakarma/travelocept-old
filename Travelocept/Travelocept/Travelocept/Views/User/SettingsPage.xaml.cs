﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Travelocept.Services.Data;
using Travelocept.ViewModels.User;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Travelocept.Views.User
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SettingsPage : ContentPage
	{
        private SettingsViewModel vm;

        public SettingsPage ()
		{
			InitializeComponent ();
            this.BindingContext = vm = new SettingsViewModel(Navigation, MongoProvider.Provider);
        }
	}
}