﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Travelocept.Services.Data;
using Travelocept.ViewModels.User;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Travelocept.Views.User
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TripsPage : ContentPage
	{
        private TripsViewModel vm;

        public TripsPage ()
		{
			InitializeComponent ();
            this.BindingContext = vm = new TripsViewModel(Navigation, MongoProvider.Provider);
        }
	}
}