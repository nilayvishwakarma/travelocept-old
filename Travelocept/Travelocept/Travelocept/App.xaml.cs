﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Travelocept.Views;
using System.Threading.Tasks;
using Travelocept.Services.App;
using Travelocept.Services.Data;
using Travelocept.Models;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Travelocept
{
    public partial class App : Application
    {
        SigninServices LoginService;

        public App()
        {
            InitializeComponent();
            LoginService = new SigninServices(MongoProvider.Provider);
            LoginWithStoredCredentials().Wait();
            MainPage = new LoginPage();
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            MainPage = new LoginPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        async Task LoginWithStoredCredentials()
        {
            try
            {
                string username = "8527907817"; // await Locker.Get("admin_username");
                string password = "password@123"; // await Locker.Get("admin_password");
                if (!(string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password)))
                {
                    var loginResult = await LoginService.SignIn(username, password);
                    switch (loginResult.Item1)
                    {
                        case LoginResult.LoginSuccessful:
                            {
                                await Task.Run(() =>
                                {
                                    Device.BeginInvokeOnMainThread(() =>
                                    {
                                        Application.Current.MainPage = new MainPage(loginResult.Item2.Role);
                                    });
                                });
                                break;
                            }
                        default:
                            {
                                await Task.Run(() =>
                                {
                                    Device.BeginInvokeOnMainThread(() =>
                                    {
                                        Application.Current.MainPage = new LoginPage();
                                    });
                                });
                                break;
                            }

                    }
                }
                else
                {
                    await Task.Run(() =>
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Application.Current.MainPage = new LoginPage();
                        });
                    });
                    // await Navigation.PushAsync(new LoginPage());
                    // App.Current.MainPage = new LoginPage();
                }

                return;
            }
            catch
            {
                await Task.Run(() =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        Application.Current.MainPage = new LoginPage();
                    });
                });
            }
        }
    }
}
