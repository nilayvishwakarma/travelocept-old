﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Travelocept.Services.App;
using Travelocept.Services.Data;
using Travelocept.Services.Native;
using Xamarin.Forms;

namespace Travelocept.Base
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        public BaseViewModel(
            INavigation navigation,
            BaseProvider provider)
        {
            this.Navigation = navigation;
            this.Provider = provider;
            this.CrudService = new CrudServices(provider);
            this.LoginService = new SigninServices(provider);
        }
        public INavigation Navigation { get; set; }
        public CrudServices CrudService { get; set; }
        public BaseProvider Provider { get; set; }
        public SigninServices LoginService { get; set; }
        public NotificationHub NotificationHub;
        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                SetProperty(ref _isBusy, value);
            }
        }

        string title = string.Empty;
        public string Title
        {
            get { return title; }
            set { SetProperty(ref title, value); }
        }


        protected bool SetProperty<T>(ref T backingStore, T value,
            [CallerMemberName]string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
