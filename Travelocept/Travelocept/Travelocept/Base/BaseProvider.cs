﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Travelocept.Base
{
    public interface BaseProvider
    {
        Task<T> FirstOrDefault<T>(
            string collectionName,
            Expression<Func<T, bool>> filter) where T : BaseModel;

        Task<T> First<T>(
            string collectionName,
            Expression<Func<T, bool>> filter) where T : BaseModel;

        Task<ObservableCollection<T>> Where<T>(
            string collectionName,
            Expression<Func<T, bool>> filter) where T : BaseModel;

        Task<ObservableCollection<T>> All<T>(
            string collectionName) where T : BaseModel;

        Task<bool> Any<T>(
            string collectionName,
            Expression<Func<T, bool>> filter) where T : BaseModel;

        Task<T> Insert<T>(
            string collectionName,
            T item) where T : BaseModel;

        Task<IEnumerable<T>> InsertRange<T>(
            string collectionName,
            List<T> items) where T : BaseModel;

        Task<T> UpdateOne<T>(
            string collectionName,
            Expression<Func<T, bool>> filter,
            T item
            ) where T : BaseModel;

        Task Update<T>(
            string collectionName,
            Expression<Func<T, bool>> filter,
            Expression<Func<T, object>> updateExpr, object value
            ) where T : BaseModel;

        Task Delete<T>(
            string collectionName,
            Expression<Func<T, bool>> filter
            ) where T : BaseModel;
    }
}
