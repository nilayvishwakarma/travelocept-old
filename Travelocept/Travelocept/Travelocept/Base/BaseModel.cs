﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Travelocept.Base
{
    public class BaseModel
    {
        public Guid Id { get; set; }
        public bool Enabled { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime ModifiedTime { get; set; }
    }

    public abstract class ModelViewModel<T> : INotifyPropertyChanged where T : BaseModel
    {
        public ModelViewModel(T model)
        {
            Model = model;
        }

        public abstract T Model { get; set; }

        public Guid Id
        {
            get
            {
                return Model.Id;
            }
            set
            {
                if (Model.Id != value)
                {
                    Model.Id = value;

                    OnPropertyChanged();
                }
            }
        }
        public bool Enabled
        {
            get
            {
                return Model.Enabled;
            }
            set
            {
                if (Model.Enabled != value)
                {
                    Model.Enabled = value;
                    OnPropertyChanged();
                }
            }
        }
        public DateTime CreatedTime
        {
            get
            {
                return Model.CreatedTime;
            }
            set
            {
                if (Model.CreatedTime != value)
                {
                    Model.CreatedTime = value;
                    OnPropertyChanged();
                }
            }
        }
        public DateTime ModifiedTime
        {
            get
            {
                return Model.ModifiedTime;
            }
            set
            {
                if (Model.ModifiedTime != value)
                {
                    Model.ModifiedTime = value;
                    OnPropertyChanged();
                }
            }
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
