﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Travelocept.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Travelocept.Controls.Dialogs
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ModifyCab : ContentPage
	{
        CabViewModel viewModel;

        public ModifyCab(Cab cab = null)
        {
            InitializeComponent();
            cab = cab ?? new Cab
            {

            };
            viewModel = new CabViewModel(cab);
            BindingContext = this.viewModel;
        }

        async void Cancel_Clicked(object sender, EventArgs e)
        {
            await App.Current.MainPage.Navigation.PopModalAsync();
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            if (viewModel.Id == Guid.Empty)
            {
                MessagingCenter.Send(this, "AddCab", viewModel.Model);
            }
            else
            {
                MessagingCenter.Send(this, "EditCab", viewModel.Model);
            }

            await App.Current.MainPage.Navigation.PopModalAsync();
        }
    }
}