﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Travelocept.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Travelocept.Controls.Dialogs
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ModifyDriver : ContentPage
	{
        DriverViewModel viewModel;

        public ModifyDriver(Driver driver = null)
        {
            InitializeComponent();
            driver = driver ?? new Driver
            {
                MobileNumber = "",
                Name = ""
            };
            viewModel = new DriverViewModel(driver);
            BindingContext = this.viewModel;
        }

        async void Cancel_Clicked(object sender, EventArgs e)
        {
            await App.Current.MainPage.Navigation.PopModalAsync();
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            if (viewModel.Id == Guid.Empty)
            {
                MessagingCenter.Send(this, "AddDriver", viewModel.Model);
            }
            else
            {
                MessagingCenter.Send(this, "EditDriver", viewModel.Model);
            }

            await App.Current.MainPage.Navigation.PopModalAsync();
        }
    }
}