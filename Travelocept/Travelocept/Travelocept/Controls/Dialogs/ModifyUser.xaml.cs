﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Travelocept.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Travelocept.Controls.Dialogs
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ModifyUser : ContentPage
	{
        LoginUserViewModel viewModel;

        public ModifyUser(Login login = null)
        {
            InitializeComponent();
            login = login ?? new Login
            {
                Role = LoginRole.User,
                MobileNumber = "",
                Password = ""
            };
            login.Password = "";
            viewModel = new LoginUserViewModel(login);
            BindingContext = this.viewModel;
        }
        async void Cancel_Clicked(object sender, EventArgs e)
        {
            await App.Current.MainPage.Navigation.PopModalAsync();
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            if (viewModel.Id == Guid.Empty)
            {
                MessagingCenter.Send(this, "AddUser", viewModel.Model);
            }
            else
            {
                MessagingCenter.Send(this, "EditUser", viewModel.Model);
            }

            await App.Current.MainPage.Navigation.PopModalAsync();
        }
    }
}