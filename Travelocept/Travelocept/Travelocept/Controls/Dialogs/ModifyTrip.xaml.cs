﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Travelocept.Models;
using Travelocept.Services.Utility;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Travelocept.Controls.Dialogs
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ModifyTrip : ContentPage
	{
        TripViewModel viewModel;

        public ModifyTrip(List<Cab> Cabs, List<Driver> Drivers, Trip trip = null)
        {
            InitializeComponent();
            trip = trip ?? new Trip
            {
                Cab = null,
                StartTime = DateTime.Now,
                OTP = Crypto.GenerateOTP()
            };
            viewModel = new TripViewModel(trip)
            {
                Cabs = Cabs,
                Drivers = Drivers
            };
            BindingContext = this.viewModel;
        }

        async void Cancel_Clicked(object sender, EventArgs e)
        {
            await App.Current.MainPage.Navigation.PopModalAsync();
        }

        async void Save_Clicked(object sender, EventArgs e)
        {
            if (viewModel.Id == Guid.Empty)
            {
                MessagingCenter.Send(this, "AddTrip", viewModel.Model);
            }
            else
            {
                MessagingCenter.Send(this, "EditTrip", viewModel.Model);
            }

            await App.Current.MainPage.Navigation.PopModalAsync();
        }
    }
}