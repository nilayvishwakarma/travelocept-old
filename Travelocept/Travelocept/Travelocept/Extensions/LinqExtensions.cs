﻿using System.Collections.ObjectModel;
using System.Linq;
using Travelocept.Base;

namespace Travelocept.Extensions
{
    public static class LinqExtensions
    {
        public static void Replace<T>(this Collection<T> items, T item) where T : BaseModel
        {
            try
            {
                var oldItem = items.FirstOrDefault(i => i.Id == item.Id);
                var oldItemIndex = items.IndexOf(oldItem);
                items[oldItemIndex] = item;
            }
            catch { }
        }
    }
}
