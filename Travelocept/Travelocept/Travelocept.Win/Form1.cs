﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Travelocept.Services.Native;

namespace Travelocept.Win
{
    public partial class Form1 : Form
    {
        private NotificationHub nHub;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            nHub = new NotificationHub("http://localhost:3000", "nilayvishwakarma");
            nHub.Recieved += NHub_Recieved;
            nHub.Connected += NHub_Connected;
        }

        private void NHub_Connected(object sender, NotificationEventArgs e)
        {
            MessageBox.Show("Connected");
        }

        private void NHub_Recieved(object sender, NotificationEventArgs e)
        {
            MessageBox.Show($"Recieved {e.Message}");
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            nHub.SendNotification("sonusingh", "Hi Sonu");
        }
    }
}
